-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 01 fév. 2019 à 13:23
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `commentaire` text NOT NULL,
  `date_commentaire` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL,
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `commentaire`, `date_commentaire`, `id_user`, `id_post`) VALUES
(1, 'Giesbert : demander lui donc à cet \"érudit\" ce qu\'est \"le pati\"  quand il veux parler de Pagnol  de la Provence , ben monsieur sais pas ?? j\'étais mdr au château de la Buzine ............................................... par contre je me suis pas expliqué pourquoi il l\'a mal pris ..................................?', '2019-02-01 09:33:55', 1, 4),
(2, 'Faire du buzz est devenu une spécialité chez certains personnages en mal de notoriété. A défaut de talent, on fait du bruit. Cela permet de se faire connaître, surtout à Paris, et d\'exister quelques temps dans les médias. Cela n\'est vraiment pas très important et ne mérite ni le temps, ni le papier que l\'on y consacre.', '2019-02-01 09:37:27', 3, 4),
(3, 'Trop bizarre ce mec!!! \r\nJe suis sûr que c\'est un vampire.', '2019-02-01 09:38:27', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `creation` datetime DEFAULT CURRENT_TIMESTAMP,
  `contenu` text,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `titre`, `creation`, `contenu`, `id_user`) VALUES
(1, 'Keanu Reeves l\'éternel!', '2019-02-01 09:26:27', 'Mais quel est donc le secret de Keanu Reeves ? A 51 ans, toujours pas la moindre ride à l\'horizon pour la star de \"Matrix\", qui cultive encore le même look qu\'au début de sa carrière. L\'acteur a tellement peu changé en vingt ans que d\'aucuns en sont venus à penser que le comédien était immortel et serait âgé de plusieurs siècles.', 1),
(2, 'Jennifer Aniston aurait conservé un peu de sperme de Brad Pitt', '2019-02-01 09:29:18', 'L\'obsession autour d\'une éventuelle grossesse de Jennifer Aniston est telle que certaines rumeurs affirment que la star de \"Friends\" aurait conservé, sur les conseils de son ancien agent, un échantillon du sperme de Brad Pitt, à l\'époque de son divorce avec l\'acteur. ', 1),
(3, 'Angelina Jolie et son frère : une relation passionnelle, à la limite de l\'inceste ?', '2019-02-01 09:31:00', 'Angelina Jolie entretient une relation fusionnelle avec son frère, James Haven. À tel point que l\'interprète de \"Tomb Raider\" avait même déclaré lors de la remise des Oscars en 2000 : \"Je suis tellement amoureuse de mon frère en ce moment.\" Le malaise a atteint son paroxysme quelques instants plus tard, lorsque l\'actrice oscarisée a embrassé son frère sur la bouche, devant tous les photographes.', 1),
(4, 'Franz Olivier Giesbert, sa position face au mouvement #MeToo', '2019-02-01 09:33:12', 'Franz Olivier Giesbert a crée la polémique en mai 2018, en évoquant le mouvement #MeToo et en remettant en cause la véracité de certains témoignages : \"Quand le soir, convoquée par Harvey Weinstein qui est dans sa robe de chambre (...) On sait très bien d\'ailleurs pourquoi il vous convoque. ', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `password`, `login`, `role`) VALUES
(1, 'user', 'user', 'user', 'user', 'user'),
(2, 'admin', 'admin', 'admin', 'admin', 'admin'),
(3, 'user1', 'user1', 'user1', 'user1', 'user');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentaire_user_fk` (`id_user`),
  ADD KEY `commentaire_post_fk` (`id_post`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_user_fk` (`id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_post_fk` FOREIGN KEY (`id_post`) REFERENCES `post` (`id`),
  ADD CONSTRAINT `commentaire_user_fk` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_user_fk` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
