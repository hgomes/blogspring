# BlogSpring

**Groupe projet: **
*  Ludovic LEGOUGNE
*  Fréderic JUBEAU
*  Henry GOMES


# Technologie:
*  Web Service : pour la liste des posts (page d'accueil)
*  MVC pour le reste.

# Instructions
*  Fichier Base de données blog.sql ()
*  Dossier BlogSpring (http://localhost:8080)
 
# Base de données
* MySQL
* Nom de la base : blog
* User: root
* Password: 


# Utilisateurs dans la base
* username: admin; password: admin (rôle admin)
* username: user; password: user (rôle user)
* username: user1; password: user1 (rôle user)

# Possibités
* Inscription et connexion des utilisateurs
* Accès à Dashbord pour admin (ajout / modification de posts)
* Ajout de commentaires pour tout utilisateur connecté
* Consultation des posts pour tous les utilisateurs


