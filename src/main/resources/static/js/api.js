//Api

(function () {
    
    //All posts
    $.ajax({
        url: 'http://localhost:8080/api/posts',
        method: 'get',
        success: function (data) {
            populate(data);
        },
        error: function (x, y, e) {
            console.log(e);
        }
    });

    function populate(data) {
        $.each(data, function (i, post) {
            var $list_posts = $('#list-posts');
            var $post_preview = $('<div class="post-preview"></div>');
            var $post_title = $('<h2></h2>');
            var $anchor = $('<a href=/post/' + post.id + '></a>');
            var $post_meta = $('<small class="post-meta"></small>');

            $post_title.text(post.titre);
            $anchor.html($post_title);
            var d = new Date(post.creation);
            $post_meta.text('Ajoutez le ' + d.toLocaleDateString() + " à " + d.toLocaleTimeString());
            $post_preview.append($anchor);
            $post_preview.append($post_meta);
            $list_posts.append($post_preview);
        });
    }
})();
