/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.apis;

import com.blog.entities.Post;
import com.blog.repositories.CommentaireRepository;
import com.blog.repositories.PostRepository;
import com.blog.repositories.UserRepository;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Henry
 */

@RestController
@CrossOrigin
@RequestMapping("/api")
public class PostApi {
    @Autowired
    PostRepository postRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    CommentaireRepository comRepo;

    @RequestMapping(value = "/posts", method = RequestMethod.GET)    
    public ResponseEntity<List<Post>> index() {
        List<Post> posts = postRepo.findAll();
        if (posts.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(posts, HttpStatus.OK);
        }
        
    }

    @RequestMapping(value="post/{id}", method = RequestMethod.GET) 
    public ResponseEntity<Post> detail(@PathVariable("id") int id) {
        try{
            Post post = postRepo.findById(id).get();
            return new ResponseEntity<>(post, HttpStatus.OK);
        }catch(NoSuchElementException e){
           return new ResponseEntity("Ce Post n'existe pas",HttpStatus.NOT_FOUND); 
        }          
    }

}
