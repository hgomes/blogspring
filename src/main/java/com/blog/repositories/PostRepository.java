/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.repositories;

import com.blog.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Henry
 */
public interface PostRepository extends JpaRepository<Post, Integer> {
    
}
