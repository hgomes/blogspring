/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.repositories;

import com.blog.entities.Commentaire;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Henry
 */
public interface CommentaireRepository extends JpaRepository<Commentaire, Integer> {
    
    @Query("SELECT c FROM Commentaire c WHERE c.idPost.id = ?1")
    List<Commentaire> findAllByPostId( int postId); 
}
