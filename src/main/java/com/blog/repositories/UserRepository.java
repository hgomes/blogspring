/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.repositories;

import com.blog.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

/**
 *
 * @author Henry
 */
@Component
public interface UserRepository extends JpaRepository<User, Integer>  {
    @Query("SELECT u FROM User u WHERE u.login = ?1")
    User findUserByLogin(String login); 
    
    @Query("SELECT u FROM User u WHERE u.login = ?1 AND u.password = ?2")
    User findUserByLoginAndPassword(String login, String password); 
}
