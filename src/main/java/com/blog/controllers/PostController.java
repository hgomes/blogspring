/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.controllers;

import com.blog.entities.Commentaire;
import com.blog.entities.Post;
import com.blog.entities.User;
import com.blog.repositories.CommentaireRepository;
import com.blog.repositories.PostRepository;
import com.blog.repositories.UserRepository;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Henry
 */
@Controller
@RequestMapping("")
public class PostController {

    @Autowired
    PostRepository postRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    CommentaireRepository comRepo;

    @RequestMapping("")
    public String index(ModelMap model) {
        List<Post> posts = postRepo.findAll();
        model.addAttribute("posts", posts);
        return "post/index";
    }

    @RequestMapping("post/{id}")
    public ModelAndView detail(ModelMap model, @PathVariable("id") int id) {
        try {
            Post post = postRepo.findById(id).get();       
            List<Commentaire> comments = comRepo.findAllByPostId(id);
            model.addAttribute("comments", comments);
            model.addAttribute("post", post);
            return new ModelAndView("post/detail", model);
            
        } catch (NoSuchElementException e) {
            List<Post> posts = postRepo.findAll();
            model.addAttribute("posts", posts);            
            return new ModelAndView("redirect:/", model);
        }
    }

    @PostMapping("addComment")    
    public ModelAndView addComment(
            ModelMap model, 
            @RequestParam("postId") String postId, 
            @RequestParam("commentaire") String comment,
            HttpServletRequest request) 
    {   
        
        if(comment.trim().equals("")){
           return new ModelAndView("redirect:post/" + postId); 
        }
        Commentaire com = new Commentaire();
        com.setCommentaire(comment); 
        int id = Integer.parseInt(postId);
        
        Post post = postRepo.findById(id).get();
        com.setIdPost(post);
        com.setDateCommentaire(new Date());
        
        User u =(User) request.getSession().getAttribute("user");
        User user = userRepo.findById(u.getId()).get(); //utiliser la session    
        com.setIdUser(user);
        
        comRepo.save(com);
        
        return new ModelAndView("redirect:post/" + id);
    }

}
