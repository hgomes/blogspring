package com.blog.controllers;

import java.util.List;
import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.blog.entities.Post;
import com.blog.entities.User;
import com.blog.repositories.PostRepository;
import com.blog.repositories.UserRepository;
import java.util.Date;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    PostRepository postRepo;

    @Autowired
    UserRepository userRepo;

    @RequestMapping("/index")
    public String index(ModelMap model) {
        List<Post> posts = postRepo.findAll();
        model.addAttribute("posts", posts);
        return "/admin/index";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView edit(ModelMap model, @PathVariable("id") int id) {
                      
        try {
            Post post = postRepo.findById(id).get(); 
            model.addAttribute("post", post);
            return new ModelAndView("admin/edit", model);
        } catch (NoSuchElementException e) {
            List<Post> posts = postRepo.findAll();
            model.addAttribute("posts", posts);
            return new ModelAndView("redirect:/admin/index", model);
        }
    }

    @PostMapping("/updatePost")
    public ModelAndView updatePost(           
            ModelMap model,
            @RequestParam("postId") String postId,
            @RequestParam("titre") String titre,
            @RequestParam("contenu") String contenu,
            HttpServletRequest request) {        
        
        int id = Integer.parseInt(postId);
        Post post = postRepo.findById(id).get();
        post.setTitre(titre);
        post.setContenu(contenu);
        postRepo.save(post);
        return new ModelAndView("redirect:/admin/index");
    }
    
    @RequestMapping("/add")
    public String add() {
       return "admin/add";
    }
    
    @PostMapping("/addPost")
    public ModelAndView addPost(
            ModelMap model,            
            @RequestParam("titre") String titre,
            @RequestParam("contenu") String contenu,
            HttpServletRequest request) {       
        Post post = new Post();
        post.setTitre(titre);
        post.setContenu(contenu);
        post.setCreation(new Date());
        post.setIdUser((User)request.getSession().getAttribute("user"));
        postRepo.save(post);
        return new ModelAndView("redirect:/admin/index");
    }

}
