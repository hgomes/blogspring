/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blog.controllers;

import com.blog.entities.User;
import com.blog.repositories.UserRepository;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Henry
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepo;

    @RequestMapping("/inscription")
    public String index() {
        return "user/index";
    }

    @PostMapping("/inscription")
    public ModelAndView inscription(
            @RequestParam("login") String login,
            @RequestParam("nom") String nom,
            @RequestParam("prenom") String prenom,
            @RequestParam("password") String password,
            @RequestParam("role") String role,
            @Valid User user,
            BindingResult result, ModelMap model) {

        //Login existe déjà   
        User usr = userRepo.findUserByLogin(login);
        
        if(usr!=null){
            model.addAttribute("error", "Ce login existe déjà.");
            return new ModelAndView("/user/index", model);
        }
        
        if(!role.trim().equalsIgnoreCase("admin") &&!role.trim().equalsIgnoreCase("user")){
            model.addAttribute("error", "Le rôle choisit n'existe pas.");
            return new ModelAndView("/user/index", model);
        }
        
        if (result.hasErrors()) {
            model.addAttribute("error", "Erreur lors de la saisie.");
            return new ModelAndView("/user/index", model);
        } else {
            User u = new User();
            u.setLogin(login.trim());
            u.setNom(nom.trim());
            u.setPrenom(prenom.trim());
            u.setPassword(password.trim());
            u.setRole(role.trim());
            userRepo.save(u);
            return new ModelAndView("redirect:/user/connexion", model);
        }
    }

    @RequestMapping("/connexion")
    public String connexion() {
        return "/user/connexion";
    }
    @RequestMapping("/deconnexion")
    public ModelAndView deconnexion(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return new ModelAndView("redirect:/");
    }

    @PostMapping("/connexion")
    public ModelAndView connexion(
            @RequestParam("login") String login,
            @RequestParam("password") String password,
            @Valid User user,
            BindingResult result, ModelMap model,
            HttpServletRequest request) {
        User u = userRepo.findUserByLoginAndPassword(login, password);
        if (u==null) {
            model.addAttribute("error", "L'identifiant et/ou mot de passe est incorrect.");
            return new ModelAndView("/user/connexion", model);
        } else {
            request.getSession().setAttribute("user", u);            
            return new ModelAndView("redirect:/", model);
        }
    }
}
